// Fill out your copyright notice in the Description page of Project Settings.

#include "RTSCameraController.h"
#include "CameraPawnBase.h"
#include "RTSGameMode.h"

#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"

void ARTSCameraController::BeginPlay()
{
    Super::BeginPlay();

    GameMode = Cast<ARTSGameMode>(GetWorld()->GetAuthGameMode());

    if (GameMode)
    {
        CameraPawn = Cast<ACameraPawnBase>(GameMode->GetCameraPawn());

        check(GetCameraPawn());  // Fail! Pawn cast to ACameraPawnBase - FAIL!
    }

    SetCanUpSpeed(true);
}

void ARTSCameraController::PlayerTick(float DeltaTime)
{
    Super::PlayerTick(DeltaTime);
}

void ARTSCameraController::SetupInputComponent()
{
    Super::SetupInputComponent();
    if (!InputComponent) return;

    /** Movement inputs: */
    InputComponent->BindAxis("MoveForward", this, &ARTSCameraController::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &ARTSCameraController::MoveRight);
    InputComponent->BindAction("Sprint", IE_Pressed, this, &ARTSCameraController::UpSpeed);
    InputComponent->BindAction("Sprint", IE_Released, this, &ARTSCameraController::DownSpeed);

    /** View: */
    InputComponent->BindAxis("LookUp", this, &ARTSCameraController::LookUp);
    InputComponent->BindAxis("Turn", this, &ARTSCameraController::Turn);

    /** Panning: */
    InputComponent->BindAction("Panning", IE_Pressed, this, &ARTSCameraController::TogglePanning);
    InputComponent->BindAction("Panning", IE_Released, this, &ARTSCameraController::TogglePanning);
    InputComponent->BindAction("ResetPanning", IE_Pressed, this, &ARTSCameraController::ResetPanning);

    /** Zoom: */
    // Zoom in:
    InputComponent->BindAction("ZoomIn", IE_Pressed, this, &ARTSCameraController::ZoomIn);
    // InputComponent->BindAction("ZoomIn", IE_Released, this, &ARTSCameraController::TogglePanning);
    // Zoom out:
    InputComponent->BindAction("ZoomOut", IE_Pressed, this, &ARTSCameraController::ZoomOut);
    // InputComponent->BindAction("ZoomOut", IE_Released, this, &ARTSCameraController::TogglePanning);
    // Reset Zoom:
    InputComponent->BindAction("ResetZoom", IE_Pressed, this, &ARTSCameraController::ResetZoom);

}

ACameraPawnBase* ARTSCameraController::GetCameraPawn() const
{
    return CameraPawn;
}

void ARTSCameraController::SetCanMove(bool IsCan)
{
    bIsCanMove = IsCan;
}

bool ARTSCameraController::IsCanMove() const
{
    return bIsCanMove;
}

bool ARTSCameraController::IsCanPanning() const
{
    return bIsCanPanning;
}

float ARTSCameraController::GetMovementSpeedCalculate() const
{
    if (!CameraPawn && !CameraPawn->SpringArm) return 0.0f;

    const auto CurrentArmLenght = CameraPawn->SpringArm->TargetArmLength;

    return FMath::Clamp(CurrentArmLenght * CameraPawn->SpeedCoefAtHeight,  //
        CameraPawn->MinMovementSpeed,                                      //
        CameraPawn->MaxMovementSpeed);                                     //
}

void ARTSCameraController::MoveForward(float Amount)
{
    // Check: Pawn is valid, Can move and Amount is nearly Zero?
    if (!CameraPawn || !IsCanMove() || FMath::IsNearlyZero(Amount, 0.0f)) return;

    CalculateMoveForward(Amount);
}

void ARTSCameraController::CalculateMoveForward(float Amount)
{
    const auto CurrentSpeedCoef = CameraPawn->CurrentSpeedCoef;
    const auto Direction = FVector((CurrentSpeedCoef * GetMovementSpeedCalculate() * Amount), 0.0f, 0.0f);
    if (Direction.IsZero()) return;
    const auto TransformDirection = UKismetMathLibrary::TransformDirection(CameraPawn->GetTransform(), Direction);
    const auto CurrentPawnLocation = CameraPawn->GetActorLocation();

    CameraPawn->SetActorLocation(FVector(CurrentPawnLocation.X, CurrentPawnLocation.Y, 100.0f) + TransformDirection);
}

void ARTSCameraController::MoveRight(float Amount)
{
    // Check: Pawn is valid, Can move and Amount is nearly Zero?
    if (!CameraPawn || !IsCanMove() || FMath::IsNearlyZero(Amount, 0.0f)) return;

    CalculateMoveRight(Amount);
}

void ARTSCameraController::CalculateMoveRight(float Amount)
{
    const auto CurrentSpeedCoef = CameraPawn->CurrentSpeedCoef;
    const auto Direction = FVector(0.0f, CurrentSpeedCoef * GetMovementSpeedCalculate() * Amount, 0.0f);
    if (Direction.IsZero()) return;

    const auto TransformDirection = UKismetMathLibrary::TransformDirection(CameraPawn->GetTransform(), Direction);
    const auto CurrentPawnLocation = CameraPawn->GetActorLocation();

    CameraPawn->SetActorLocation(FVector(CurrentPawnLocation.X, CurrentPawnLocation.Y, 100.0f) + TransformDirection);
}

void ARTSCameraController::UpSpeed()
{
    if (!CameraPawn || bIsCanPanning) return;

    if (bIsUpSpeed)
    {
        CameraPawn->CurrentSpeedCoef = CameraPawn->UpSpeedCoef;
    }
    else
    {
        CameraPawn->CurrentSpeedCoef = CameraPawn->NormalSpeedCoef;
    }
}

void ARTSCameraController::DownSpeed()
{
    CameraPawn->CurrentSpeedCoef = CameraPawn->NormalSpeedCoef;
}

void ARTSCameraController::SetCanUpSpeed(bool Result)
{
    bIsUpSpeed = Result;
}

void ARTSCameraController::LookUp(float Amount)
{
    // Y
    if (bIsCanMove && !IsCanPanning())
    {
        const FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());

        FVector2D MousePosition;
        if (!GetWorld()->GetGameViewport()->GetMousePosition(MousePosition)) return;
        GEngine->AddOnScreenDebugMessage(0, 0.0f, FColor::Red, (TEXT("Mouse postion: %s"), *MousePosition.ToString()));

        auto RelativeY = MousePosition.Y / ViewportSize.Y;

        const auto MinY = CameraPawn->MinYForMove;
        const auto MaxY = CameraPawn->MaxYForMove;

        if (MaxY >= RelativeY && RelativeY >= MinY) return;

        auto Speed = CameraPawn->SpeedMoveY;

        if (RelativeY >= MaxY)
        {
            Speed *= -1;
        }

        Speed *= CameraPawn->CurrentSpeedCoef;
        // TO DO:
        // Fix bug! Bad move to up!
        CameraPawn->AddActorLocalOffset(FVector(Speed, 0.0f, 0.0f), true);
    }

    if (!CameraPawn || FMath::IsNearlyZero(Amount)) return;

    if (IsCanPanning())
    {
        const auto CurrentPawnRotation = CameraPawn->GetActorRotation();

        float NewPitch = FMath::Clamp(CurrentPawnRotation.Pitch + (CameraPawn->PanSensitivity * Amount), -15.0f, 45.0f);

        auto NewPawnRotation = FRotator(NewPitch, CurrentPawnRotation.Yaw, CurrentPawnRotation.Roll);

        CameraPawn->SetActorRotation(NewPawnRotation);
    }
}

void ARTSCameraController::Turn(float Amount)
{
    // X

    if (bIsCanMove && !IsCanPanning())
    {
        const FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());

        FVector2D MousePosition;
        if (!GetWorld()->GetGameViewport()->GetMousePosition(MousePosition)) return;

        auto RelativeX = MousePosition.X / ViewportSize.X;

        const auto MinX = CameraPawn->MinXForMove;
        const auto MaxX = CameraPawn->MaxXForMove;

        if (MaxX >= RelativeX && RelativeX >= MinX) return;

        auto Speed = CameraPawn->SpeedMoveX;

        if (RelativeX <= MaxX)
        {
            Speed *= -1;
        }

        Speed *= CameraPawn->CurrentSpeedCoef;

        CameraPawn->AddActorLocalOffset(FVector(0.0f, Speed, 0.0f), true);
    }

    if (!CameraPawn || FMath::IsNearlyZero(Amount)) return;

    if (IsCanPanning())
    {
        const auto CurrentPawnRotation = CameraPawn->GetActorRotation();

        float NewYaw = CurrentPawnRotation.Yaw + (CameraPawn->PanSensitivity * Amount);

        auto NewPawnRotation = FRotator(CurrentPawnRotation.Pitch, NewYaw, CurrentPawnRotation.Roll);

        CameraPawn->SetActorRotation(NewPawnRotation);
    }
}

void ARTSCameraController::ZoomIn()
{
    if (!CameraPawn) return;

    auto CurrentArmLength = CameraPawn->SpringArm->TargetArmLength;
    auto CurrentZoomSensitivity = CameraPawn->ZoomSemsitivity;
    auto MinArmDistance = CameraPawn->MinArmDistance;

    CameraPawn->SpringArm->TargetArmLength = FMath::Clamp(CurrentArmLength - CurrentZoomSensitivity, MinArmDistance, CurrentArmLength);
}

void ARTSCameraController::ZoomOut()
{
    if (!CameraPawn) return;

    auto CurrentArmLength = CameraPawn->SpringArm->TargetArmLength;
    auto CurrentZoomSensitivity = CameraPawn->ZoomSemsitivity;
    auto MaxArmDistance = CameraPawn->MaxArmDistance;

    CameraPawn->SpringArm->TargetArmLength = FMath::Clamp(CurrentArmLength + CurrentZoomSensitivity, CurrentArmLength, MaxArmDistance);
}

void ARTSCameraController::ResetZoom()
{
    if (!CameraPawn) return;

    CameraPawn->SpringArm->TargetArmLength = CameraPawn->DefaultArmDistance;
}

void ARTSCameraController::TogglePanning()
{
    bIsCanPanning = !bIsCanPanning;
    bIsCanMove = !bIsCanPanning;

    // I Find BuG:
    // need set this:
    SetCanUpSpeed(bIsCanMove);
}

void ARTSCameraController::ResetPanning()
{
    if (!CameraPawn)  //|| UKismetMathLibrary::EqualEqual_FloatFloat(CameraPawn->GetActorRotation().Pitch, CameraPawn->PanningPitchBase),
        // 0.001f)
        return;

    auto NewRotation = CameraPawn->GetActorRotation();
    NewRotation.Pitch = CameraPawn->PanningPitchBase;

    CameraPawn->SetActorRotation(NewRotation);
}
