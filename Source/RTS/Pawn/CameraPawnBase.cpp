// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraPawnBase.h"

#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
ACameraPawnBase::ACameraPawnBase()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    /** Initial Components: */

    // Create Scene component:
    Scene = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
    this->SetRootComponent(Scene);  // And Set root component - Scene.

    // Create Sphere:
    Sphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Spere"));
    Sphere->SetupAttachment(GetRootComponent());

    // Create SpringArm:
    SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
    SpringArm->SetupAttachment(Sphere);

    // Creat Camera:
    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    Camera->SetupAttachment(SpringArm);

    /** Set defaults settings : */

    // Movement:
    NormalSpeedCoef = 1.0f;
    UpSpeedCoef = 2.0f;
    CurrentSpeedCoef = NormalSpeedCoef;
    SpeedCoefAtHeight = 0.01f;
    MinMovementSpeed = 5;
    MaxMovementSpeed = 20;

    // Panning:
    PanSensitivity = 1;
    PanningPitchBase = 0.0f;

    // Zoom:
    ZoomSemsitivity = 50.0f;
    MaxArmDistance = 2500.0f;
    

    // Movement with Mouse:
    MaxXForMove = 0.975f;
    MinXForMove = 0.025f;
    SpeedMoveX = 5.0f;

    MaxYForMove = 0.975f;
    MinYForMove = 0.025f;
    SpeedMoveY = 5.0f;
     
}

// Called when the game starts or when spawned
void ACameraPawnBase::BeginPlay()
{
    Super::BeginPlay();

    DefaultArmDistance = SpringArm->TargetArmLength;
}

// Called every frame
void ACameraPawnBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ACameraPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}
