// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CameraPawnBase.generated.h"

class USceneComponent;
class UStaticMeshComponent;
class USpringArmComponent;
class UCameraComponent;

UCLASS()
class RTS_API ACameraPawnBase : public APawn
{
    GENERATED_BODY()

public:
    // Sets default values for this pawn's properties
    ACameraPawnBase();

    /** Components: */
    UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Components")
    USceneComponent* Scene = nullptr;
    UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Components")
    UStaticMeshComponent* Sphere = nullptr;
    UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Components")
    USpringArmComponent* SpringArm = nullptr;
    UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Components")
    UCameraComponent* Camera = nullptr;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    /** Movement: */

    /** Spped coefficient at Spring Arm length: */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement",
        meta = (ClampMin = "0.01", ClamMax = "1.0", UIMin = "0.01", UIMax = "1.0"))
    float SpeedCoefAtHeight = 0.0f;

    /** Spped coefficient */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement | SpeedCoef", meta = (ClampMin = "0.5", UIMin = "0.5"))
    float CurrentSpeedCoef = 0.0f;
    /** Spped coefficient */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement | SpeedCoef", meta = (ClampMin = "0.5", UIMin = "0.5"))
    float NormalSpeedCoef = 0.0f;
    /** Spped coefficient */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement | SpeedCoef", meta = (ClampMin = "0.5", UIMin = "0.5"))
    float UpSpeedCoef = 0.0f;

    // Min Movement speed:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
    float MinMovementSpeed = 0.0f;

    // Max Movement speed:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
    float MaxMovementSpeed = 0.0f;

    /** Zoom: */
    /** ZoomSemsitivity: */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Zoom")
    float ZoomSemsitivity = 0.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Zoom | Distance", meta = (ClampMin = "250.0", UIMin = "250.0"))
    float MaxArmDistance = 0.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Zoom | Distance",
        meta = (ClampMin = "0.0", ClampMax = "250.0", UIMin = "0.0", UIMax = "250.0"))
    float MinArmDistance = 0.0f;

    float DefaultArmDistance = 0.0f;

    /** Movement with Mouse: */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
    float MinYForMove = 0.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
    float MaxYForMove = 0.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement", meta = (ClamMin = "0.0", UIMin = "0.0"))
    float SpeedMoveY = 0.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
    float MinXForMove = 0.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
    float MaxXForMove = 0.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement", meta = (ClamMin = "0.0", UIMin = "0.0"))
    float SpeedMoveX = 0.0f;

    /** Panning: */

    /** Base Panning postion: */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Panning")
    float PanningPitchBase = 0.0f;

    /** Spped Panning sensitibity: */
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Panning",
        meta = (ClampMin = "0.01", ClamMax = "1.0", UIMin = "0.01", UIMax = "1.0"))
    float PanSensitivity = 0.0f;
};
