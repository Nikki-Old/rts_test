// Copyright Epic Games, Inc. All Rights Reserved.

#include "RTSGameMode.h"

#include "RTSCameraController.h"
#include "CameraPawnBase.h"
#include "RTSGameState.h"
#include "RTSPlayerState.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

ARTSGameMode::ARTSGameMode()
{
    // use our custom CameraController class
    PlayerControllerClass = ARTSCameraController::StaticClass();

    // set default pawn class to our
    DefaultPawnClass = ACameraPawnBase::StaticClass();

    // Set Base HUD:
    HUDClass = ARTSGameState::StaticClass();

    // Set Base Player State:
    PlayerStateClass = ARTSPlayerState::StaticClass();
}

ACameraPawnBase* ARTSGameMode::GetCameraPawn() const
{
    return Cast<ACameraPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}
