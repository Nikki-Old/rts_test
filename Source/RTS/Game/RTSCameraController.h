// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RTSCameraController.generated.h"

/**
 *      Class for contro CameraPawnBase
 */

class ACameraPawnBase;
class ARTSGameMode;

UCLASS()
class RTS_API ARTSCameraController : public APlayerController
{
    GENERATED_BODY()

public:

    ARTSCameraController();

    // Begin PlayerController interface
    virtual void BeginPlay() override;
    // virtual void PlayerTick(float DeltaTime) override;
    virtual void SetupInputComponent() override;
    // End PlayerController interface

public:
    /** Return Current Camera Pawn: */
    UFUNCTION(BlueprintPure, Category = "Pawn")
    ACameraPawnBase* GetCameraPawn() const;

    /** Set Can Move Pawn: */
    UFUNCTION(BlueprintCallable, Category = "Pawn | Movement")
    void SetCanMove(bool IsCan);

    /** Get CanMove: */
    UFUNCTION(BlueprintPure, Category = "Pawn | Movement")
    bool IsCanMove() const;

    /** Get CanPanning: */
    UFUNCTION(BlueprintPure, Category = "Pawn | Movement | Panning")
    bool IsCanPanning() const;

private:
    /** Movement: */

    // Calculate:
    float GetMovementSpeedCalculate() const;

    // Move to forward:
    void MoveForward(float Amount);
    void CalculateMoveForward(float Amount);


    // Move to right:
    void MoveRight(float Amount);
    void CalculateMoveRight(float Amount);

    bool bIsCanMove = true;

    void UpSpeed();
    void DownSpeed();

    void SetCanUpSpeed(bool Result);

    bool bIsUpSpeed = false;

    /** View: */
    void LookUp(float Amount);

    void Turn(float Amount);

    /** Zoom: */
    void ZoomIn();
    void ZoomOut();
    void ResetZoom();

    /** Panning */

    void TogglePanning();

    void ResetPanning();

    bool bIsCanPanning = false;

    /** Reference: */
    ACameraPawnBase* CameraPawn = nullptr;
    ARTSGameMode* GameMode = nullptr;
};
