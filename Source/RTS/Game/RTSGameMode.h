// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RTSGameMode.generated.h"

class ACameraPawnBase;

UCLASS(minimalapi)
class ARTSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARTSGameMode();

	/** Get Camera Pawn: */
	ACameraPawnBase* GetCameraPawn() const;
};



